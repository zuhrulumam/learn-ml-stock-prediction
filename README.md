 # Learn ML 
 
 1. Day 1
    - download csv data http://files.statworx.com/sp500.zip
    - install pandas
    - install keras
    - install tensorflow
    
 2. Day 2
    - install matpotlib
    - install scikit-learn
    - create training and test data (with sequentially sliced) 
        for note to search "There are a lot of different approaches to time series cross validation, such as rolling forecasts with and without refitting or more elaborate concepts such as time series bootstrap resampling. The latter involves repeated samples from the remainder of the seasonal decomposition of the time series in order to simulate samples that follow the same seasonal pattern as the original time series but are not exact copies of its values."
    - scale the data using sklearn
        Most neural network architectures benefit from scaling the inputs (sometimes also the output). Why? Because most common activation functions of the network’s neurons such as tanh or sigmoid are defined on the [-1, 1] or [0, 1] interval respectively. Nowadays, rectified linear unit (ReLU) activations are commonly used activations which are unbounded on the axis of possible activation values. However, we will scale both the inputs and targets anyway. Scaling can be easily accomplished in Python using sklearn’s MinMaxScaler.
